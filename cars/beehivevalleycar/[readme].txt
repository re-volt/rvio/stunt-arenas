Car information
================================================================
Car name: Timothy's Car
Car Type: Remodel
Installed folder: ...\cars\beehivevalleycar
Description: 

This car is part of the track "Beehive Valley Adventures".

Author Information
================================================================
Remodel: Kiwi
Texture: Kiwi
Parameters: Kiwi
 
Construction
================================================================
Base model: Seructiva (by Kipy and Mladen007), Cheesy Racer (by Kipy), Artair (by Mighty Cucumber and Kiwi)
Base parameters: Artair (by Mighty Cucumber and Kiwi)
Editor(s) used: Notepad++, Blender (with ReVolt-Plugin from Marv), PhotoImpact12
 
Copyright / Permissions
================================================================
You may do whatever you want with this car, as long as you mention the original author in the credits.

Version 1.0 from June 7th, 2019











O  R  I  G  I  N  A  L     R  E  A  D  M  E


=====================================================================
=====================================================================
=====================================================================
=====================================================================
Details:

Name:                          Seructiva
Authors:                Mladen007 & Kipy
Category:                       Original
Class:                          Semi-pro    
Engine type:                        Glow
Trans:                               4WD
Top Speed:          36.9 mph / 59.4 km/h
Folder's name: .../ReVolt/cars/seructiva/

=====================================================================
=====================================================================
=====================================================================
=====================================================================
=====================================================================

Tools:

Blender 2.69 & 2.76 + jig's plugin
Paint.NET
prm2hul
autoshade CarLightning

=====================================================================
=====================================================================
Credits:

Copyright belong to Mladen and Kipy.
You can repaint/modify, but ALWAYS mention the authors of the car.

=====================================================================
=====================================================================
							2018.01.29.
=====================================================================
=====================================================================










Car information
================================================================
Car name: Artair
Car Type: Remodel
Top speed: 66 kph
Rating/Class: 5 (Pro)
Installed folder: ...\cars\artair
Description: 

This is Artair, a remodel of Sakyû by Xarc.
The car has a very soft suspension, and a Pro-class rating.

Params by Mighty, remodel and texture by Kiwi.

Have fun!
Kiwi & Mighty Cucumber

Author Information
================================================================
Remodel: Kiwi
Texture: Kiwi
Parameters: MightyCucumber
 
Construction
================================================================
Base model: Sakyû (by Xarc)
Editor(s) used: Notepad++, Blender (with ReVolt-Plugin from Marv), PhotoImpact12
 
Copyright / Permissions
================================================================
You may do whatever you want with this car, as long as you mention us and the original author in the credits.

Version 1.1 from December 28th, 2018