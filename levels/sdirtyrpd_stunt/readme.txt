﻿                    +-------------------------****--------------------------+
====================|	Simple Dirty Raceway Playground (Stunt Arena only)  |====================
##||#####||#####||##|       	     Custom Track for Re-Volt	    	    |##||#####||#####||##
====================|                 By VaidX47 (VaiDuX461)                |====================
                    +-------------------------****--------------------------+
More tracks by me: http://revoltzone.net/search.php?term=VaiDuX461&db3=on
For updates and other stuff, check: www.mediafire.com/?dbg9nyxxnwc16

NOTE: Incomplete version for stunt arena only purposes!
Get the full track from Re-Volt Zone or my Mediafire folder.

                                          /-*********-\
-========================================- Information -========================================-

Name: Simple Dirty Raceway Playground
Release date: 2012-11-03
Folder name: SDirtyRPd_stunt
Track Type: Stunt Arena
Stunt Arena Stars: 25
Custom Features: Loading screen
Bonus Features: Yes (rain)

Latest RVGL patch is recommended to play this track. (Oldest compatible - v1.2 Alpha 12.0815)

                                          /-*********-\
-========================================- Description -========================================-

Nothing really fancy here. It is what it says: just simple, dirty, raceway playground.
The track was made with JimK's Off-Road kit, there are almost no texture modifications.
What is so special about it is that you can play in any game mode, which is I believe the first
Re-Volt extreme track ever to do this. Besides game modes, there are some bonus stuff.

Arena layout is quite open, so that's a great point for Battle Tag and Stunt Arena modes.
It includes various ramps, a pool with water, some trees, objects like: bottles, beach ball and cones.
Good way to just simply mess around in the track.
As for racing layout, it is really short and simple. I couldn't make the raceline very long due to game's limited engine.
Stunt Arena and Battle Tag needs open areas, and supporting all games modes is a difficult task to do.

This was my first ever publicly released track for Re-Volt.

Features:

 -25 Stunt Arena stars to collect
 -Rain option (on/off, off by default)
 -3 secrets ("Easter eggs") to find with F6 (free view) camera

                                         /-**********-\
-=======================================- Installation -========================================-

Extract all archive contents to your main Re-Volt directory.
Default directory: "C:\Program Files (x86)\Acclaim Entertainment\Re-Volt".

                                        /-************-\
-======================================- Bonus Features -=======================================-

 -Rain (on/off) - Enables rain, makes the sky darker and adds rainy background sounds. Reminiscent of "Toytanic 2".
	(default off)

Copy SDirtyRPd_stunt.fob file from "rain" folder to "custom" to enable it.

                                        /-************-\
-======================================- Uninstallation -=======================================-

Delete 'SDirtyRPd_stunt.bmp' file inside 'gfx' folder and 'SDirtyRPd_stunt' inside 'levels'.

You can also use WolfR4 (http://jigebren.free.fr/games/pc/re-volt/wolfr4/) to delete tracks.

                                          /-********-\
-========================================- Change log -=========================================-

[This stunt arena only release is based on v1.01]

v1.01 [2017-10-09]:
-Added `DIFFICULTY` parameter, it will now show "Easy" in the track selection screen for racing modes
 (since RVGL 17.1009a build)

v1.0  [2012-11-03]:
-First release

                                         /-**********-\
-=======================================- Known issues =========================================-
Known bugs which are impossible or too difficult to fix for me.

 -Some hardly noticeable holes on the ground (visible blue skybox). No effect to the player car.
 -There are 2 or 3 places where textures collide, they cause overlapping. Not really annoying.

                                            /-****-\
-==========================================- Trivia -===========================================-

 -First Re-Volt track to be compatible with all game modes: Single Race, Stunt Arena, Battle Tag...
 -Arena was meant to be only played on Stunt Arena and Battle Tag modes, the track was called "Dirt Track Arena Battle".
 -Original track was rebuilded at least 2 times, because it had too many graphical glitches.
   Racelines were slightly different. (Archives are lost)
 -Track development was on hold for a whole year. Main layout was done in 2011, finished in 2012.
 -Topdown camera idea was influenced from Dave-o-rama's "Canyon of the Wolves" track.

                                           /-*****-\
-=========================================- Credits -===========================================-

Track design, layout, building and minor texture editing
	VaidX47 (formerly VaiDuX461)

Off-Road Arena PRM kit, textures
	JimK

Batch tool
	VaidX47

rvtmod7 (rvglue)
	Gabor Varga, Alexander Kroller, Christer Sundin

Original game
	Acclaim Studios London & Acclaim Entertainment

Hardware which was used to make this
	"Sh*tty computer"

Readme design
	VaidX47

Special thanks
	Huki & Jigebren (Re-Volt 1.2)
	Irfan Skiljan (IrfanView)

	    Thanks to YOU for downloading and everyone else from the Re-Volt community

                                             /-***-\
-===========================================- Links -===========================================-

http://rvgl.re-volt.io                    - RVGL patch (formerly v1.2) home page
http://re-volt.io                         - Game news, downloads, documentation and more
http://re-volt.io/discord                 - Re-Volt related Discord servers (The most active in English is called Re-Volt)
http://revoltzone.net                     - Site for downloading additional custom content for Re-Volt
http://www.revoltxtg.co.uk                - Other place for game content downloads
http://forum.re-volt.io                   - The current forum of Re-Volt and RVGL project

http://www.revoltxtg.co.uk/msf/nutrkx/simple_dirty_raceway.htm - RVXTG track review

                                          /-*********-\
-========================================- Permissions -========================================-

You can redistribute this archive in any electronic format WITHOUT making any modifications.
If the author can be mentioned before downloading this archive, please use their name and no one else.
IT MUST NOT TO BE SOLD FOR ANY COMMERCIAL PURPOSES!

Editing AND redistributing is only allowed when you contact me and ask for my permission first.
I can be contacted on Discord through RV server, Re-Volt forums (preferably The Re-Volt Hideout) and 
email: vaidux461@gmail.com
If I won't respond anywhere in 6 months, that means you are free to do it.

If something was used or inspired from this archive, please mention original authors in your work.
Blatantly copying things such as: track layout and name IS FORBIDDEN. Always contact me if you are unsure.

_________________________________________«««===~&~===»»»_________________________________________
Excuse me if there are any grammar errors
Have fun!
©2011-2012, 2020 VaidX47
vaidux461@gmail.com
Track ver 1.01S