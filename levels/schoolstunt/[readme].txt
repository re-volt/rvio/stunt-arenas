  _________      .__                  .__/\         ________          __  ._.
 /   _____/ ____ |  |__   ____   ____ |  )/  ______ \_____  \  __ ___/  |_| |
 \_____  \_/ ___\|  |  \ /  _ \ /  _ \|  |  /  ___/  /   |   \|  |  \   __\ |
 /        \  \___|   Y  (  <_> |  <_> )  |__\___ \  /    |    \  |  /|  |  \|
/_______  /\___  >___|  /\____/ \____/|____/____  > \_______  /____/ |__|  __
        \/     \/     \/                        \/          \/             \/
		                          - STUNTS -
									
                                 R E A D M E
                       Version 1.1, from April 4th 2021

  S U M M A R Y
═════════════════════════════════════════════════════════════════════════════
Track Name:         School's Stunts!
Game Type:			Stunt Arena
Folder Name:        schoolstunt
Author:             Kiwi
Creation:           March to April 2021
Difficulty:         Medium
Stunt Arena Stars:  64


  T R A C K    D E S C R I P T I O N
═════════════════════════════════════════════════════════════════════════════

This is a big Stunt Arena, using the complete School's Out! 2 area. Some 
parts were adjusted/added/removed compared to the racetrack.

The Stunt Arena is designed to played with my new car "SchoolVolt II", which
is also included in the download. All stars can be catched with this car! You
for sure can also use other cars, but I recommend doing the first play-through
with SchoolVolt II.

Major features of School's Stunts!:

+ 64 Stars to find and catch
+ 45 minutes of finest House music
+ Playtime between 1 and 2 hours
+ Included car: SchoolVolt II

Have fun!
  Kiwi


  C O M P A T I B I L I T Y
═════════════════════════════════════════════════════════════════════════════
+ This track is only compatible with RVGL 21.0125a or later. It's not
  compatible with the original (Vanilla) Re-Volt or Re-Volt 1.2.
+ For best performance and increased graphic quality, it's highly recommended
  to use the shader renderer. Set 'shaders = 1' in 'rvgl\profiles\rvgl.ini' 
  where 'rvgl' is the location of your RVGL installation.


  K N O W N    I S S U E S
═════════════════════════════════════════════════════════════════════════════
Issues caused by not using the shader renderer:

+ A much higher chances to get framedrops.
+ Graphical issues, especially flickering translucent faces and lights.

Issues caused by using an outdated RVGL version:

+ Issues with missing lights/shadows when not using RVGL version 19.1001a or
  later. The limit for lights was increased from 256 to 1024 in this version.
+ Issues with missing textures when using an RVGL version before 19.0120a.
  The support for up to 64 texture pages was introduced in this version.


  T R I V I A
═════════════════════════════════════════════════════════════════════════════
+ The high school is named after the people who created the game concept
  for Re-Volt in the late 90s: Paul Phippen and Simon Harrison.
+ The skateboard design with the half-naked woman was part of Re-Volt's
  Alpha and Demo versions, and was replaced by the orange texture in the
  final versions.
+ The red "Re-Volt" graffiti located in the sewer is taken from a texture 
  sheet which was made for a level named "Council Estate". The level didn't
  made it into the final release of Re-Volt.
+ The street names "Sheep Lane" and "Basil Road" were also present in Gabor
  Varga's track PetroVolt. Also the name of the fictional company in the 
  track Quake! was called "Basil Sheep Ltd". 
+ The logo from the fictional high school basketball team "Angry Pangas" is
  based on a picture of Panga which can be found in the gallery image lib4b.


  C R E A T I O N
═════════════════════════════════════════════════════════════════════════════
Used Tools:      + Blender 2.79b with Marv's Re-Volt Plugin
                 + Ulead Photo Impact 12
                 + World Cut 11-11-11 by jigebren
                 + mkmirror by Ali
                 + Audacity 2.2.2
                 + Notepad++ v7.9.1
                 + RVGL Makeitgood mode
				 
W/PRM Polys:     ~ 110.000
NCP Polys:       ~  70.000

The ViziBoxes (schools1.vis) were made by Mr. Burguers.

I used some meshes and textures from other tracks as a basis, and adjusted
them so they fitted my needs. They are listed below. Meshes and textures
which are not listed are made by me from scratch or are modified versions 
from free sources on the internet. Re-used models from the original Re-Volt 
tracks (made by Probe/Acclaim) are also not listed.

Used meshes:    + Bus: School-Volt by Xarc; Phat Trucker by MightyCucumber
                + Van: Special Delivery by FrenchFry
                + Chair: Quake! by Gabor
                + Computer: Quake! by Gabor
                + Copier: Quake! by Gabor
                + Lamp: PetroVolt by Gabor
                + Manhole cover: PetroVolt by Gabor	
                + Skatepark ramps: Skatepark F20 by Alexander
                + Working site sign: PetroVolt by Gabor
                + Teacher table: Quake! by Gabor
                + Pupil table: Quake! by Gabor
                + Water dispenser: Quake! by Gabor
                + Boat: Venice by Gabor
                + Vent shaft fan: Jailhouse Rock by Human
                + Sewer base: Jailhouse Rock by Human
                + Guard railing: PetroVolt by Gabor
                + Plants in pots: Quake! by Gabor
                + Spray bottle: Grisville by Allan1
                + Grass: Grisville by Allan1
                + Waste bag: Grisville by Allan1

Used textures:  + Bus: School-Volt by Xarc; Phat Trucker by MightyCucumber
                + Van: Special Delivery by FrenchFry
                + Chair: Quake! by Gabor
                + Computer: Quake! by Gabor
                + Copier: Quake! by Gabor
                + Skate Park: Skatepark F20 by Alexander
                + Working site sign: PetroVolt by Gabor
                + Teacher table: Quake! by Gabor
                + Water dispenser: Quake! by Gabor
                + Street name signs: PetroVolt by Gabor
                + Construction area sign: PetroVolt by Gabor
                + Classroom walls/windows/ceiling: Quake! by Gabor
                + Plants in pots: Quake! by Gabor
				+ Some of the graffiti in the sewer: Industry by Marv & r6te
                + Spray bottle: Grisville by Allan1
                + Waste bag: Grisville by Allan1

                + Supermarket boxes: Taken from Zeino's box contest
                    - Yeezy Bois by Alexander
                    - Runko by Xarc
                    - Duck with rice by Kiwi
                    - Champignonship by 607
                    - Oats by G_J
                    - Raccoon Raisin by Mushy
                    - Salt by Gotolei

The "Gotolei, "Stvictorar", "Whitedoom" and "Zipperzbieracz" graffiti were 
made using the free online graffiti-generator at graffiticreator.net

Used Sounds:    + animal1.wav			 *5
                + arcade.wav             *1
                + bats.wav               *1
                + eagle1.wav             *1
                + escalate.wav           *2
                + helicopter_loop.wav    *3
				+ intamb1.wav            *1
				+ museumam.wav           *1
				+ thunder1.wav			 *4

*1: sounddogs.com, Unknown licence type
*2: soundbible.com, Sampling Plus 1.0 Licence
*3: bigsoundbank.com, Licensed under a Creative Commons "Attr." 4.0 Licence
*4: soundbible.com, Attribution 3.0 Licence
*5: Ranch by Marv and r6te

Used Music:

"Liveset 2006" by Boris Brechja (c) 2013
School's Stunts!-Remix (c) 2021 by Kiwi

For You, For Everybody, For Free
https://www.borisbrejcha.de/downloads

  T H A N K    Y O U
═════════════════════════════════════════════════════════════════════════════
Thank you to the original Probe developers for creating Re-Volt, and thank
you to Huki & the rest of the RVGL team for keeping the game alive 20 years
after its initial release.

Thanks to Gabor Varga, whose tracks inspired me a lot to do School's Out!

                     ----------------------------
                V E R Y   S P E C I A L    T H A N K S
                     ----------------------------

+ Alexander         For the skatepark meshes and textures
+ Instant			For the backpack and gym bench meshes
+ Gotolei			For doing the shading
+ Mr. Burguers      For doing the ViziBoxes


  R E L E A S E   H I S T O R Y
═════════════════════════════════════════════════════════════════════════════
Version 1.1 from April 1st 2021
 + Bugfix release
	- Corrected issues with translucent boxes in Supermarket freezer
	- Corrected invisible wall in school staircase
	- Adjusted location and jump to one star
	- Replaced skybox
	- New preview picture
	- Removed thunder SFX
	
Version 1.0 from April 1st 2021
 + Initial public release on Re-Volt World (www.revoltworld.net)

 
  D I S C L A I M E R
═════════════════════════════════════════════════════════════════════════════
You are free to distribute this track, but please don't change any parts
of it without my approval. Contact me on TRH forum.

You are free to use parts of the track (models, textures, sounds, ...)
for your own purposes, but don't forget to credit the original authors.