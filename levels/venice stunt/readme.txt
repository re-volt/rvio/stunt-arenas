
Track info
----------
Track name:	Venice
Type:		Stunt
Original Author:	Gabor Varga
Stunt Author:	Allan1
E-mails:		[bootes@freemail.hu] n [allan.mitestainer@yahoo.com.br]


Description
-----------
Take a tour to the lovely city of Venice (in Italy, not California ;)).
The streets are narrow, and in many places nothing prevents you from falling
into the filthy water.


Installation / uninstallation
-----------------------------

Install folder: venice
Installation:   unzip Venice.zip into your Re-Volt directory.
Uninstallation: delete <Re-Volt dir>\levels\venice
                delete <Re-Volt dir>\gfx\venice.bmp
                delete <Re-Volt dir>\gfx\venice.bmq


Playing Stunt
----------------

Warning! To play Venice track in stunt arena mode you need to install wolfr4: [http://jigebren.free.fr/jeux/pc/revolt/WolfR4.html].
Launch Re-volt in Wolfr4, this is very important.
Enter in 'Stunt Arena' and you find Venice in the stunts list (on TV).
The "stunt.dat" file is also used to save the star progression for each track. To reset the progression for one track,
replace its "stunt.dat" file with an empty one (it can be done directly from the contextual menu in the tracks list).


Legals
------
Everything in Venice is made from scratch; some textures are based on material
that is shipped with 3DSMax.
You can use, copy and distribute this package; you can use any part of it
(meshes, textures) unchanged or modified in your own work - provided you
do it for free and mention Gabor as the original author.


Acknowledgements
----------------
Thanks to Acclaim for making this wonderful game.
Thanks to the following people who made the creation of Venice possible:
- Alexander Kroeller and Steven Ellis, who wrote the tools which convert 3DSMax
  output into Re-Volt levels.
- Andrea Barbagallo, who translated to Italian and gave advice on
  Italian / Venetian matters. (Unfortunately, only a little of his work can be
  seen in the track, because I have run out of texture space.)
- My wife Agnes, who helped me in the design of some textures.
- Everyone at RacersPoint, for the cheerings and encouragement - and for just
  being there.


Has a good luck and a very fun!
Allan1
