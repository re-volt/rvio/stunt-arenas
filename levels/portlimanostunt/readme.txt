  _____   _____   ______ _______             _____ _______ _______ __   _  _____       _______ _______ _     _ __   _ _______ _______
 |_____] |     | |_____/    |         |        |   |  |  | |_____| | \  | |     |      |______    |    |     | | \  |    |    |______
 |       |_____| |    \_    |         |_____ __|__ |  |  | |     | |  \_| |_____|      ______|    |    |_____| |  \_|    |    ______|
                                                                                                                                     
--------------------------------------------------------------------------------------------------

hi there, im benderfan3124 and this is a collection of tracks i've made this summer. i hope you enjoy.
there is not much to them but i will try to make a simple description for all of them.

port limano stunts is happening over the whole port limano! your mission is to collect all 30 limited edition chubaqua drink bottles that are hidden around the map.
for an experienced stuntman this should take about 10 minutes at most. the stars are all possible with stock pros. have fun!

credits:
music is RW - Head Rush by rowan-willard https://www.newgrounds.com/audio/listen/1149174
textures from internet
https://freesound.org/people/lebaston100/sounds/243629/
https://freesound.org/people/mcharchenko/sounds/271144/
https://freesound.org/people/Smokey9977/sounds/569564/
https://freesound.org/people/Kinoton/sounds/561815/
https://freesound.org/people/Ornitorrinco/sounds/329709/
https://freesound.org/people/seth-m/sounds/341922/
https://freesound.org/people/waweee/sounds/370967/
https://freesound.org/people/juskiddink/sounds/98479/
tropic raceway as the base for the water particles