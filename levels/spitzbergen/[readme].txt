    .----..----. .-. .---.  .---. .----. .----..----.  .---. .----..-. .-.
   { {__  | {}  }| |{_   _}{_   / | {}  }| {_  | {}  }/   __}| {_  |  `| |
   .-._} }| .--' | |  | |   /    }| {}  }| {__ | .-. \\  {_ }| {__ | |\  |
   `----' `-'    `-'  `-'   `---' `----' `----'`-' `-' `---' `----'`-' `-'
									
                                 R E A D M E
                     Version 1.0, from February 20th 2022

  S U M M A R Y
═════════════════════════════════════════════════════════════════════════════
Track Name:         Spitzbergen
Game Type:			Stunt Arena
Folder Name:        spitzbergen
Author:             Kiwi
Creation:           February 2022
Difficulty:         Medium
Stunt Arena Stars:  50


  T R A C K    D E S C R I P T I O N
═════════════════════════════════════════════════════════════════════════════

This is a big Stunt Arena located on the island of Svalbard. Svalbard (also
known as Spitsbergen or Spitzbergen) is a Norwegian archipelago in the
Arctic Ocean. North of mainland Europe, it is about midway between the
northern coast of Norway and the North Pole.

This map was made to take part at the Re-Volt World Birthday Contest 2022.

Designed to be played with the included car "Explorer". All 50 stars can be
catched with this car. Not tested with other cars than "Explorer"!

It's strongly recommended to use all the available camera views while
exploring this map. Especially at the uphill areas one of the onboard cameras
is the best choose.

Major features of Spitzbergen:

+ 50 Stars to find and catch
+ 30 minutes of included ambient music
+ Explore the inside of Toytanic!
+ Playtime between 1 and 2 hours
+ Included car: Explorer

Have fun!
  Kiwi


  C O M P A T I B I L I T Y
═════════════════════════════════════════════════════════════════════════════
+ This track is only compatible with RVGL 21.0930a or later. It's not
  compatible with the original (Vanilla) Re-Volt or Re-Volt 1.2.
+ For best performance and increased graphic quality, it's highly recommended
  to use the shader renderer. Set 'shaders = 1' in 'rvgl\profiles\rvgl.ini' 
  where 'rvgl' is the location of your RVGL installation.


  K N O W N    I S S U E S
═════════════════════════════════════════════════════════════════════════════
Issues caused by not using the shader renderer:

+ A much higher chances to get framedrops.
+ Graphical issues, especially flickering translucent faces and lights.

Known limitations:

+ At the Toytanic no car shadow and skidmarks are visible.
+ No camera smoothing at the Toytanic (No "camera collision")


  C R E A T I O N
═════════════════════════════════════════════════════════════════════════════
Used Tools:      + Blender 2.79b with Marv's Re-Volt Plugin
                 + Ulead Photo Impact 12
                 + Audacity 2.2.2
                 + Notepad++ v8.1.9.1
                 + RVGL Makeitgood mode
				 
W/PRM Polygons:  ~ 98.000
NCP Polygons:    ~ 90.000

I used some meshes and textures from other sources as a base, and adjusted
them so they fitted my needs. They are listed below. Meshes and textures
which are not listed are made by me from scratch. Re-used models from the
original Re-Volt tracks (made by Probe/Acclaim) are also not listed.

Used meshes:    + Jeep: Toyota Land Cruiser (J100) by Skarma
					- https://www.revoltworld.net/dl/landcruiser100
					- Adjusted and downscaled texture
					- Slightly adjusted 3d model
					
                + Unimog: Mercedes-Benz Unimog 1300 by Skarma
				    - https://www.revoltworld.net/dl/unimog
					- Adjusted and downscaled texture
					- Slightly adjusted 3d model
					
				+ House: Forest Loner in Norway Diorama by Tom Verbeeck
					- https://skfb.ly/6YuBV
					- Only used one of the houses
					- Adjusted and downscaled texture
					- Adjusted 3d model
					
				+ Skidoo: Doctor Who The Adventure Games ripped by DinnerSonic
				    - https://www.models-resource.com/pc_computer/doctorwho/model/1883/
					- Adjusted and downscaled texture
					- Slightly adjusted 3d model
					
				+ Mountain: Snow Mountain by hkp941111
					- https://skfb.ly/6yGGu
					- Adjusted and downscaled texture
					- Adjusted 3d model
					
				+ Spray bottle: Grisville by Allan1
				+ Plants in pots: Quake! by Gabor

Used textures:  + Skybox by Tubers, adjusted by Kiwi
				+ Water animation by Javildesign, adjusted by Kiwi

Used Sounds:    + animal1.wav			 sounddogs.com
                + animal2.wav            sounddogs.com
                + birds1.wav           	 Unimog by Skarma
                + birds2.wav			 freesoundslibrary.com
				+ rattler.wav            bigsoundbank.com

sounddogs.com: Unknown licence type
bigsoundbank.com: Licensed under a Creative Commons "Attr." 4.0 Licence
freesoundslibrary.com: Attribution 4.0 International (CC BY 4.0)

Used Music:

"A Collection Of Songs About Norway" by dronæment (c) 2011
Spitzbergen-Remix (c) 2022 by Kiwi
FMA link: https://freemusicarchive.org/music/dronment/A_Collection_of_Songs_about_Norway


  T H A N K    Y O U
═════════════════════════════════════════════════════════════════════════════
Thank you to the original Probe developers for creating Re-Volt, and thank
you to Huki & the rest of the RVGL team for keeping the game alive 20 years
after its initial release.

                     ----------------------------
                V E R Y   S P E C I A L    T H A N K S
                     ----------------------------

+ Tubers	        For the skybox
+ Skarma			For the Land Cruiser and Unimog models
+ Javildesign		For some technical workaround-suggestions


  R E L E A S E   H I S T O R Y
═════════════════════════════════════════════════════════════════════════════
Version 1.0 from February 20th 2022
 + Release for the Re-Volt World Birthday Contest (www.revoltworld.net)

 
  D I S C L A I M E R
═════════════════════════════════════════════════════════════════════════════
You are free to distribute this track, but please don't change any parts
of it without my approval. Contact me on Discord.

You are free to use parts of the track (models, textures, sounds, ...)
for your own purposes, but don't forget to credit the original authors.