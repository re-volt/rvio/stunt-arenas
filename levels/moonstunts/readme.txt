====================
STUNTS ON THE MOON
====================
Full Track Name: Stunts on the Moon
Track Difficulty: Unknown
Track Length: Unknown
Track Author: Shredster7
Special Thanks: JimK for his Warehouse and Offroad Kits!
====================
BACKSTORY:
The Toy-Volt R/C Cars are officially the first toys to reach the moon! The astronauts that brought them there have set up a antigravity stunt track for the little toy carz to run wild in. They even hid the customary 20 stars within the arena, to see how well the R/C's can drive on another planet... wait, hasn't this been done before by someone else? 
====================
DESCRIPTION:
That loop-de-loop in front of you isn't a full loop. Instead, it will lead to a small section of the arena with full-blown reverse gravity! Don't bother trying to leave the boundaries, as you'll be pushed back by some Farce Fields if you go too far. Interesting Fact: the Probe UFO will float in place on this track! It seems that a Velocity Field aimed straight up with a MAG of 1,000 will cause the UFO to float indefinately. Spooky!
====================
CREDITS:
Special thanks to JimK for the Offroad Kit, which the majority of this track is made of, as well as his Warehouse Kit, which the loops and a certain hidden star could not be done without.
====================
ENJOY AND THANKS FOR DOWNLOADING!
====================