Content:
>>> Track information
>>> Changelog
>>> Description
>>> Contact
>>> Tools used
>>> Textures used and thanks
>>> Disclaimer

Track information:
>>> Name: Two neighborhoods Stunts
>>> Folder name: nhoods_stunts
>>> Author: Keyran
>>> Year of first release: 2021
>>> Year of last update: 2022
>>> Category: Stunt Arena
>>> Stars: 55
>>> Tested with rvgl20.0430a

Changelog:
Version 1.1:
+ Fixed a collision bug close to the river from Toys in the Hood 2.
+ Slightly decreased the total amount of NCP polygons (only in some straight lines).
+ Moved the start position to a more friendly place, which is:
--> Closer to the center of the arena
--> The player never have to do a u-turn after repositioning
--> It is still in the upper part of the track
+ Slightly moved a hidden star because it was possible to get it in a different way that ruins the pleasure of finding it.

Version 1.1.1:
+ Fixed a major collision bug at the flat bridge in the TITH2 area that appeared in the previous update.

Version 1.1.2:
+ Fixed a z-fighting issue in a window.
+ Improved shading in the bedroom.

Description:
This is a stunt arena for Two Neighborhoods.
There are 38 stars I an get with stock Rookie cars (I was very lucky with one of them!)
You should be able to get all the other stars with stock Pro cars.

Contact:
>>> Discord: Keyran#3667

Tools used:
>>> Photofiltre 7 for textures
>>> Blender with Marv's plugin
>>> RVGL (Makeitgood)
>>> WorldCut

Textures used and thanks:
Textures a, b, c, d, e, f, g, h, i, j, k, l, p, q and u are stock textures.
Textures m, n, r, s, t, v and x are likely in public domain (I haven't found any restriction on them). I modified texture r.
Texture o has stock textures, and on the left, a texture made by myself from textures m and n.
Texture w is scratch made by me and contains the rvgl logo.
All instances and models are either stock or made by me. Some stock models are slightly modified to take into account the location of their corresponding textures.

Thanks Paperman who changed the colors of most of the new textures and edited vertex painting in the world file, the national roads, the grass and the cliff look better.
Thanks Acclaim / Probe for the original game, and the community that keeps the game alive.

Disclaimer:
Don't use any part of this track for commercial purposes (except the textures m, n, r, s, t, v and x).