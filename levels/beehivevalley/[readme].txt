                ____     ___    ___  __ __  ____  __ __    ___
               |    \   /  _]  /  _]|  |  ||    ||  |  |  /  _]
               |  o  ) /  [_  /  [_ |  |  | |  | |  |  | /  [_
               |     ||    _]|    _]|  _  | |  | |  |  ||    _]
               |  O  ||   [_ |   [_ |  |  | |  | |  :  ||   [_
               |     ||     ||     ||  |  | |  |  \   / |     |
               |_____||_____||_____||__|__||____|  \_/  |_____|
                   __ __   ____  _      _        ___  __ __
                  |  |  | /    || |    | |      /  _]|  |  |
                  |  |  ||  o  || |    | |     /  [_ |  |  |
                  |  |  ||     || |___ | |___ |    _]|  ~  |
                  |  :  ||  _  ||     ||     ||   [_ |___, |
                   \   / |  |  ||     ||     ||     ||     |
                    \_/  |__|__||_____||_____||_____||____/
      ____  ___    __ __    ___  ____   ______  __ __  ____     ___  _____
     /    ||   \  |  |  |  /  _]|    \ |      ||  |  ||    \   /  _]/ ___/
    |  o  ||    \ |  |  | /  [_ |  _  ||      ||  |  ||  D  ) /  [_(   \_
    |     ||  D  ||  |  ||    _]|  |  ||_|  |_||  |  ||    / |    _]\__  |
    |  _  ||     ||  :  ||   [_ |  |  |  |  |  |  :  ||    \ |   [_ /  \ |
    |  |  ||     | \   / |     ||  |  |  |  |  |     ||  .  \|     |\    |
    |__|__||_____|  \_/  |_____||__|__|  |__|   \__,_||__|\_||_____| \___|


                                     b y

                  W h o   s h o t   t h e   K i w i b i r d
                       E n t e r t a i n m e n t   L t d .

                               ( c )   1 9 9 9



                                  R E A D M E
                        Version 1.2, from June 12th 2019

  S U M M A R Y
════════════════════════════════════════════════════════════════════════════
Track Name:         Beehive Valley Adventures
Folder Name:        beehivevalley
Author:             Kiwi
Creation:           May to June 2019
Type:               Stunt Arena
Stars:              64


  T R A C K    D E S C R I P T I O N
════════════════════════════════════════════════════════════════════════════
You know Conker, Banjoo-Kazooie, Croc or Gex? You think it is not possible
to turn Re-Volt into a late-90's 3D platformer like the ones listed above?

Beehive Valley Adventures is a homage to the games of my youth and
everything inside this map comes right out of my diseased brain - which was
maybe caused by exact this games? ;-)

Choose either Timothy the Mouse or Wolly the Sheep to start your adventure 
today! Pick up diamonds in Hedgehog Hills. Meet a naked water nymph in Fairy
River. Collect magic mushrooms at the Mushroom Mountains. Perform crazy
boat-jumps in Popcorn Plains. Fall down the narrow platforms at The
Swampland and smash your gamepad out the window after the 100th try. Yay!

Depending on your choose (Timothy's car or Wolly's boat), the experience
will feel different. Not all quests can be done with both of them, so choose
wisely. 

Please only use the included vehicles to get full joy. Some of the quests 
are pretty tricky, so don't start whining. This is how videogames were like
back then, you remember? ;)

Major features of Beehive Valley Adventures:

+ Big stunt arena with 5 different areas to explore
+ 1 car and 1 boat included
+ Meet several sexist talking and cursing characters
+ Collect 64 diamonds and solve other quests
+ 2-5 hours of play time, depending on your skill
+ A lot of customizations (sounds, moving objects...)
+ Oldschool soundtrack by Heatley Bros

Screenshots and videos of the map can be found here: 
https://kiwis.rv.gl/tracks/originals/beehivevalley

Have fun! :)
  Kiwi

The vehicles are based on cars by Kipy.
Timothy is based on a character made by Kipy.
Some of the textures are courtesy of DC.All.
Music by Heatley Bros. https://www.youtube.com/user/HeatleyBros

Beehive Valley Adventures is rated "M" for crude and sexual content, 
bad language and brief nudity.


  C R E A T I O N
════════════════════════════════════════════════════════════════════════════
The basis of this map is "Lego", made with the Re-Volt Track Editor. The
tdf-file (bhv.tdf) is included in the download and can be loaded in the
Track Editor, if you copy it to the programs ./tdf folder. The Lego-raceline
is 100% unmodified. The landscape (inside the .w) was added in Blender and
all decoration (as .prm-instances or .m-objects) was added with Makeitgood.

Tools I've used:    + Track Editor 1.1a15.0301 by Kenny
                    + Blender 2.79b with Marv's Re-Volt Plugin
                    + prm2hul 10-10-15 by jigebren
                    + World Cut 11-11-11 by jigebren
                    + Ulead Photo Impact 12
                    + Audacity 2.2.2
                    + Notepad++ v7.6.6
                    + RVGL Makeitgood mode

W/PRM Polygons:     ~ 70.000
NCP Polygons:       ~ 30.000
Instance Models:    33
Total Instances:    1024 (Limit reached)
Total Objects:      256 (Limit reached)

I used some meshes and textures from other tracks as a basis, and adjusted
them to fit my needs. They are listed below. Meshes and textures which are
not listed are made by me from scratch or are from free sources from the
internet.

Used meshes:        + Windmill, Hay Bale: Windmill Plains ripped by DC.All
                    + Bridge: Holiday Camp by Romeo
                    + Flowers: CTR Palm Marsh by Killer Wheels
                    + Fish, Duck: Floating World by RST

Used textures:      + Grass, Water, Stones, Snow: Diddy Kong Racing by Rare
                    + Waterfall animation: Donut Plains 3 by DC.All
                    + Windmill, Cornfield: Windmill Plains ripped by DC.All
                    + Skybox: Windmill Plains ripped by DC.All
                    + Flowers: CTR Palm Marsh by Killer Wheels
                    + Trees: Super Mario 64 by Nintendo
                    + Fish: Floating World by RST

Used Sounds:        + petrol.wav: Donut Plains 3 by DC.All
                    + tropics3.wav: Windmill Plains ripped by DC.All
                    + animal1.wav: Ranch by Marv and R6te
                    + Rest: http://sounddogs.com

Used Music:

"Happy Day" by Heatley Bros
"8 bit Summer" by Heatley Bros
"Heart Power" by Heatley Bros
"8 bit Win" by Heatley Bros

Writer: Brett Heatley, ASCAP
Publisher: Heatley Music Publishing, ASCAP
Label: Kyzen Music
https://www.youtube.com/user/HeatleyBros


  I N F O R M A T I O N S
════════════════════════════════════════════════════════════════════════════
+ This track is only compatible and tested with RVGL
+ For best performance it's highly recommended to use RVGL's Shader Edition
+ If you have collected all the 64 diamonds and want to start-over, just
  delete the "beehivevalley.stunt"-file in your profiles folder.


  K N O W N    I S S U E S
════════════════════════════════════════════════════════════════════════════
+ Possible graphical issues when using Non-Shader Edition of RVGL,
  especially with transparency effects.


  T H A N K    Y O U
════════════════════════════════════════════════════════════════════════════
Thank you to the original Probe developers for creating Re-Volt, and thank
you to Huki for keeping the game alive 20 years after its initial release
by still improving and maintaining RVGL.

Special Thanks to Brett Heatley, DC.All, Kipy, Trixed, and my wife Bianca.


  R E L E A S E   H I S T O R Y
════════════════════════════════════════════════════════════════════════════
Version 1.2 from June 12th, 2019
 + Bugfix release:
   Corrected issue with Repo-trigger behind Fairy River Waterfall
Version 1.1 from June 7th, 2019
 + Bugfix release:
   Added some more Repo-triggers, adjusted lights, adjusted trackpic.
Version 1.0 from June 7th, 2019
 + Initial public release, released on revoltzone.net


  D I S C L A I M E R
════════════════════════════════════════════════════════════════════════════
You are free to distribute this track, but please don't change any parts of
it without my approval. Contact me at TRH forum or by mail: kiwi[AT]rv.gl

You are free to use parts of the tracks (models, textures, sounds, ...)
for your own purposes, but don't forget to credit the original authors.