
  E X P L O R E R
  
  Version 1.0 from February 12th, 2022

================================================================
  Car information
================================================================

Date      : 12/02/2022
Car Name  : Explorer
Author    : Kiwi
Car Type  : Remodel
Folder    : ...\cars\explorer
Top Speed : 61 kph
Mass      : 3,0 KG
Rating    : 4 (Semi-Pro)

Explorer was made for the Re-Volt World Birthday Contest 2022,
as part of my Stunt Arena entry named "Spitzbergen". The car is
not meant for Singleplayer- or Multiplayer-races, nor for Time
Trialing.

It come with an alternate skin, with the cameras detached.


================================================================
  Construction
================================================================

Polygons       : 2310 (Body, Cameras, Axles, Shocks, Wheels)

Base           : Sasquatch by Kiwi and Skarma

Editors used   : Blender 2.79b
                 Ulead Photo Impact 12
                 Audacity
                 Notepad++


================================================================
  Thank You
================================================================

+ Skarma for the Sasquatch base parameters
+ The Re-Volt World Discord server for motivating me


================================================================
  Copyright / Permissions
================================================================

Please do not change any parts of this car without asking, as
long it's not for your personal use. You can reuse any parts of
this car for your own car, as long you mention the original
creators accordingly.
