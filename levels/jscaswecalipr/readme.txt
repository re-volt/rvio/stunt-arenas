Name: Sweet California (Practice Mode)
Authors: Josh Scorpius
Length: n/a
Released on: Saturday, March 30, 2019.
Updated on: Saturday, March 30, 2019.
Stars to found: 40

This is the practice mode of the Sweet California circuit. An free roam map to find the stars.

Any case, contact at: 
Josh Scorpius: joshscorpius94@gmail.com