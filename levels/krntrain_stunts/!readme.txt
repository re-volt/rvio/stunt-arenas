﻿Content:
>>> Install Train Stunts
>>> Track information
>>> How to contact me
>>> Description
>>> Known issues
>>> Tools used
>>> Thanks and credits
>>> Disclaimer

Install Train Stunts:
Unzip Train Stunts into the main folder of the game.

/!\ IMPORTANT /!\ Even if you have had performance issues with Train Adventure, this arena should work fine, because there is no other car.

Track information:
>>> Name: Train Stunts
>>> Folder name: krntrain_stunts
>>> Author: Keyran
>>> Development time: December 2022 - June 2023
>>> Year of first release: 2023
>>> Category: Stunt Arena
>>> Difficulty: Extreme
>>> Stars: 35

Contact:
On Discord: I am Keyran#3667

Description:
You were on a very long journey with some friends and had some races in the train.
Now they get out of the train, but you still haven't reached your destination.
You'll have to find something else to do during your journey.
Of course, you could look at the landscape, but after 3 minutes it will be boring.
What about trying some stunts with your car?

The train has now a cruise speed of 324 kph!

More relaxing music because you can also go on the top of the train (or wherever you want) and just look at the landscape.

This stunt arena is designed and tested for stock Pro cars (Cougar, Humma, Toyeca, AMW and Panga).
You will have to use several cars to get all the stars.

Known issues:
Collision with obstacles between the trains are not realistic. I can't do anything to fix that.

Tools used:
>>> Photofiltre for textures
>>> Blender with Marv's plugin.
>>> RVGL (Makeitgood)
>>> WorldCut
>>> Visual Studio Code for custom animations and editing the inf file.
>>> https://ttsmp3.com/text-to-speech/British%20English/ for the voices (more precisely Amy and Matthew). It doesn't require to login, it just shows some adds.

Thanks and credits:
>>> Acclaim and Probe for the original game.
>>> CapitaineSZM, MightyCucumber and rodik for testing this track and giving me feedback and suggestions of improvement.
>>> Textures and models:
Toilets and sofa (I gave it a different texture) are from Quake! by Gabor.
The suitcase and its textures are from Airport 1 by Xarc. I have removed some useless faces.
The bottom left texture on krntraing is from The Great Silence by Kiwi (it is unused).

The train door is under the Creative Commons Attribution-Share Alike 4.0 International licence (https://creativecommons.org/licenses/by-sa/4.0/deed.en)
You can find more information about it here: https://commons.wikimedia.org/wiki/File:Mind_The_Step_sign_on_doors_at_NGR_trains_in_Brisbane,_Queensland.jpg

The red carpet texture (krntrainba) is from webtreats on Flickr (https://www.flickr.com/photos/webtreatsetc/5541566294/) and under the Creative Commons Attribution 2.0 Generic licence (https://creativecommons.org/licenses/by/2.0/)

The Nazca picture is under the Creative Commons Attribution-Share Alike 2.5 Spain licence (https://creativecommons.org/licenses/by-sa/2.5/es/deed.en)
Original readme for this picture (in Spanish): "Copyleft :) Colegota 2006. Puedes copiar, distribuir, publicar y modificar esta fotografia bajo las condiciones expresadas en http://mapamundi.info/licenciaCCbysa"

The texture file krntrainfa.bmp has a window texture from Airport 1, by Xarc.
The texture file krntrainoa.bmp has a screenshot from the game Brave Dwarves 2 by GameOverGames.
The texture file krntrainpa.bmp has a screenshot from the game Astro Avenger 2 by Sahmon Games.
The graffiti in krntrainqa.bmp is from http://www.creativity103.com/ (http://www.creativity103.com/collections/Graffiti/Graffiti_DSC9482.jpg) and under the Creative Commons Attribution 3.0 Unported licence (CC BY 3.0) (https://creativecommons.org/licenses/by/3.0/)

The train icon seen in the loading screen is from https://pngimg.com/image/16630 and under Attribution-NonCommercial 4.0 International licence.

>>> Sounds:
Some train and ambient sounds: https://mixkit.co/

>>> Music:
Stars and Laurels by Shane Ivers - https://www.silvermansound.com

Disclaimer:
Don't use this track or its content for commercial purposes.
You can reuse any part of this track provided you mention the original authors. 