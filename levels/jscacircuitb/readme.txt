Name: JSCA Circuit (Practice Mode)
Authors: Josh Scorpius
Length: n/a
Stars to found: 27
Released on: Sunday, February 24, 2019.
Updated on: Sunday, March 3, 2019.

This is the Practice Mode of the Josh Scorpius City Area Circuit. This is a practice star version, that the mission is pick all the stars from this zone.

Contains:
Music (Colourful Mathematics, 4 minutes 45 seconds)

Any case, contact at: 
Josh Scorpius: joshscorpius94@gmail.com