Car information
================================================================
Car name: Wolly's Boat
Car Type: Remodel
Installed folder: ...\cars\beehivevalleyboat
Description: 

This car is part of the track "Beehive Valley Adventures".

Author Information
================================================================
Remodel: Kiwi
Texture: Kiwi
Parameters: Kiwi
 
Construction
================================================================
Base model: Smiter (by Kipy), 
Base parameters: Tiger Tooth (by Trixed)
Editor(s) used: Notepad++, Blender (with ReVolt-Plugin from Marv), PhotoImpact12
 
Copyright / Permissions
================================================================
You may do whatever you want with this car, as long as you mention the original author in the credits.

Version 1.0 from June 7th, 2019












O  R  I  G  I  N  A  L     R  E  A  D  M  E





=====================================================================
=====================================================================
=====================================================================
=====================================================================
Details:

Name:                             Smiter
Author:                             Kipy
Category:                       Original
Class:                            Rookie    
Engine type:                    Electric
Trans:                               4WD
Top Speed:           32,1 mph / 51,6 kph
Folder's name:    .../ReVolt/cars/smiter/

=====================================================================
=====================================================================
=====================================================================

Tools:

Blender 2.76 + jig's plugin
Paint.NET
prm2hul
autoshade CarLightning

=====================================================================
=====================================================================
Credits:

Copyright belongs to Kipy, who made the car itself.
You can repaint/modify this car, but ALWAYS mention the author of
this car.
Special thanks to Mladen, burner and FZG.

=====================================================================
=====================================================================
							2018.02.16.
=====================================================================
=====================================================================