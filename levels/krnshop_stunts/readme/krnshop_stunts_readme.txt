Content:
>>> Install Antigrav SC Stunts
>>> Track information
>>> Changelog
>>> How to contact me
>>> Description
>>> Known issues
>>> Tools used
>>> Thanks and credits
>>> Disclaimer

Install Antigrav SC Stunts:
Unzip everything into the main folder of the game, the files will be placed where they should be.

Track information:
>>> Name: Antigrav SC Stunts
>>> Folder name: krnshop_stunts
>>> Author: Keyran
>>> Year of first release: 2022
>>> Category: Stunt Arena
>>> Difficulty: Extreme
>>> Stars: 32

Changelog:
Version 1.0.1:
+ Added 4 shadows in Light mode.
+ Made the ceiling of the elevators visible from above.

Contact:
On Discord: I am Keyran#3667.

Description:
You can't enjoy the full potential of Antigrav SC without a stunt arena. Some things can't be done in races.
You will need several cars to get all the stars.

Known issues:
The camera wont be very good when driving on a wall or upside down. It depends on the cars.

Tools used:
>>> Photofiltre for textures
>>> Blender with Marv's plugin and Blender 2.93 with Dummiesman's plugin.
>>> RVGL (Makeitgood)
>>> WorldCut

Thanks and credits:
>>> Acclaim and Probe for the original game.
Supermarket textures and instances (files from krnshopa.bmp to krnshopj.bmp).
Botanical Garden plants and dirt (krnshops.bmp)
>>> Textures:
loafbrr: all the food assets you can find in this track and texture krnshopn.bmp. Link: https://loafbrr.itch.io/
If I forgot something or someone, please let me know and I will update the readme.
Texture krnshopr.bmp from https://freepngimg.com/png/70995-simple-frame-flower-motif-png-download-free and under CC BY-NC 4.0 licence (https://creativecommons.org/licenses/by-nc/4.0/)
>>> Provided music:
"Electronaut" by Aerologic
Label: Toucan Music
http://freemusicarchive.org/
Licensed under a Attribution-NonCommercial 3.0 International License: https://creativecommons.org/licenses/by-nc/3.0/
>>> The whole community, all those who keep the game alive.

Disclaimer:
Don't use this track or its content for commercial purposes.
You can reuse any part of this track provided you mention the original authors.