Track info
______________________________________________________

Name:		Present Hunt
Author:		Allan1
Type:		Stunt
End date:	11/07/30

Description:	You have to collect xmas presents in a colourful village.
		The village is made of a forklift rental company, a market, parks and a public pool.
		There are 25 presents in total, you need to be patient and cautious.
		Play in the parks, climb ramps, enter in houses and swim or dive in the pool.
		Have a merry xmas! ( 157 days before :P ).

Setup:		Extract the .zip file into your re-volt directory.
Uninstall:	Delete all extracted files. (duh.)

Reset Stunt:	WolfR4 will monitor the user tracks with a "stunt.dat" file, to make them selectable in Stunt mode.
		The "stunt.dat" file is also used to save the star progression for each track.
		To reset the progression for one track, replace its "stunt.dat" file with an empty one
		(it can be done directly from the contextual menu in the tracks list).


Custom Models
______________________________________________________

Xmas present [by hilaire9]
collect them!
 

Warnings and Hints
______________________________________________________

1. When is required to reposition the car, it will come back to the start!
2. Walls don't have camera colision
3. Use the pro cars to take presents in high jumps
4. There are some secret shortcuts in the track :P


Credits
______________________________________________________

Tools:		Paint
		Re-volt Track Editor
		MAKEITGOOD Tools
		Windows Sound Recorder
		WolfR4

Graphics:	Pat S
		Zagames

Instances:	Pat S
		RickyD
		Zagames

Models:		Hilaire9

Based on the game Prezzie Hunt by Skyline Software.



Thanks for playing my track!
Allan1
		