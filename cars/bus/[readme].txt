
  S C H O O L V O L T   I I
  
  Version 1.0 from March 27th, 2021

================================================================
  Car information
================================================================

Date      : 27/03/2021
Car Name  : SchoolVolt II
Author    : Kiwi
Car Type  : Remodel
Folder    : ...\cars\bus
Top Speed : 53 kph / 33 mph
Mass      : 2,5 KG
Rating    : 2 (Amateur)

SchoolVolt II is a remodel of Xarc's SchoolVolt, and Mighty's
Phat Trucker. I used the remodel at my School's Out! tracks
already and decided to make a standalone car out of it right
after I released the tracks. However, it took me more than
a year to finish the car.

The car comes with a custom honk sound, which I used for 
Redneck as well. The SFX is from the 1997 game "Ignition",
from the Swedish developers UDS.

Have fun!
- Kiwi


================================================================
  Construction
================================================================

Polygons       : ~1500 (Body, Axles, Wheels)

Base           : SchoolVolt by Xarc
                 Phat Trucker by Mighty Cucumber

Editors used   : Blender 2.79b
                 Ulead Photo Impact 12
                 Notepad++


================================================================
  Thank You
================================================================

+ Xarc and Mighty Cucumber for the base cars
+ UDS for the honk sound


================================================================
  Copyright / Permissions
================================================================

Please do not change any parts of this car without asking, as
long it's not for your personal use. You can reuse any parts of
this car for your own car, as long you mention the original
creators accordingly.
