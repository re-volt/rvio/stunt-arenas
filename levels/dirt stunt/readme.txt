Track information
================================================================
Track name : Dirt Stunt Arena
Track Type :Stunt/Extreme
Length : There is no length in stunt arenas.
Description : this track replaces your stunt arena. so back it up before putting in this track. also, you should reset progress tables of you already beat the stunt arena. afterwards simply download the reg cheat from manmountains site (http://freewebs.com/revolt_f1)
all jumps are 100% able to be made with toyeca.
enjoy.

Author Information
================================================================
Author Name : zipperrulez
Email Address : zipperrulez@gmail.com
also, please visit:
http://zipperrulez.co.nr
http://z3.invisionfree.com/Revolt_Live/index.php

Construction
================================================================
Editor(s) used : Re-Volt MAKEITGOOD / RVGlue

Additional Credits
================================================================
jumk for the offroad kit which was used to make this track.

Copyright / Permissions
================================================================
You may do whatever you want with this Stunt Arena. Provided you include this file, with NO modifications. No selling this track as well.