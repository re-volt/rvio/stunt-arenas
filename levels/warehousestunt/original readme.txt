Warehouse level kit by JimK
---------------------------
[This version updated by Gibbler, 27/01/00]

Notes:
* Feel free to this kit as you please (for any non commercial ventures) - but please include a JimK copyright in your readme files for the models.
* You must have some 'extreme' editing knowledge to use this kit, or you could mess up your installation of Revolt.
* I cannot be responsible for any problems you may encounter from using this kit.
* This kit needs to use the level folder 'nhood1' - Please *MAKE SURE* to backup your nhood1 level before installing this kit!
* Please remember that you have to set up your own path nodes, AI, and zones if you want to race your level! If you dont know how to do this, check out the various Revolt websites, there is everything you need to know (you just have to be patient and look!) - but as a good starting point, check out the RVExtreme site: http://dialspace.dial.pipex.com/town/street/xiz39/revolt/

Installation:
1. Rename your original "nhood1" level folder to a temporary name, such as "__nhood1".
2. Extract all files in this .zip file, using full pathnames, into your Revolt ROOT directory, eg "C:\Program Files\Revolt".

Use:
1. Start Revolt.
2. Start Race.
3. Single Race
4. Any Race Mode.
5. Player Name: MAKEITGOOD (press ENTER)
6. Go back to Race Selection screen. Move down to the new "Edit Mode:" entry and select the edit mode your require. Then press ENTER until you reach the track selection screen.
7. Select the track: Toys in the Hood 1 (press ENTER twice to start race).
8. Edit.

[Reference the RVExreme website for information on editing mode keys]


Ideas for Use:
The warehouse, by default, is a large empty space, with just the walls, roof, metal beams, a couple of lights and three doors. But there is a file called "ideas.fin" in the nhood1 folder and this has sample ideas of some of the items and how to use them! To look at this setup, you'll need to rename the current "nhood1.fin" file to a temporary name (such as "nhood1.fi-") and then rename the "ideas.fin" file to "nhood1.fin".

The posibilities of this kit are only as good as the person using it! If you want, you could get rid of the walls and beams, add in 2 or 3 more floors, and any other custom models you download. For barriers you could take the "bgwall" model and replace the crate section of the bitmap with near true-black (RGB 1,1,1) and place them in the level as invisible walls - when you are done, change the color to true-black (RGB 0,0,0) as this is invisible in the game! 

The possibilities go on and on, but hopefully you will find this a good starting point!

Texture-Mapping:
The current model texture-mapping is as follows:
Floor - nhood1a.bmp
Walls - nhood1b.bmp
Misc: Crates, Beams and doors - nhood1c.bmp
Brick Wall - nhood1d.bmp
Gravel Mound - nhood1e.bmp

If you switch the "nhood1f.bmp" with the current floor texture and the mound bitmap, you have a nice grass level.

Author Notes:
You can drive around this level in an edit-mode if you like - I had a lot of fun just doing that.
I am also working on some different themes using the same items, but with different bitmaps for them (changes the whole feel and effect of the level). 

Also check out my original cars at revolt HQ (http://www.revolthq.com/), under "JimK".
